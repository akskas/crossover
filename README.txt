Prerequisites

	1. ansible 2.2.0
	2. aws account with:
		a. pem key created on aws(key_name `akskas.pem`)
		b. aws access key and secret keys
	3. dockerhub account

Run instructions

	1. ceate a pem key on aws and paste it in ansible directory:
	2. update dockerhub credentials:
		a. In `ansible/docker/vars/main.yml`
			* update dockerhub credentials 
		b. In `ansible/group_vars/all/vars.yml`
			* update `app_image_name`
			* update project directory variables
			* make sure `isSwarmInitialized` is set to false
		c. In `ansible/infra/vars/main.yml`
			* update `ec2_access_key`, `ec2_secret_key` and `ec2_key_name`
		d. update pem key name in ansible/hosts 
	4. goto ansible and run `sudo ./start.sh`

	5. To deploy an exisiting tag,
		a. update the IS_REDEPLOY flag in ansible/start.sh
		b. update image tags in `ansible/group_vars/all/vars.yml`
		c. run `sudo ./start.sh`

Hardcoded values:
	Two values used in `Code/src/main/resources/application.properties` to connect to mysql are hardcoded
		1. mysql_host: crossover_mysql (name of mysql service )
		2. mysql_password: ankul333